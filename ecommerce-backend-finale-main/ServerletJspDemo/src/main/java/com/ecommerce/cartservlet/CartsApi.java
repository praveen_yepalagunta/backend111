package com.ecommerce.cartservlet;



import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.cart.Cart;
import com.ecommerce.products.Products;
import com.ecommerce.web.dao.ProductsDao;
import com.ecommerce.CartDao.CartDao;
import com.google.gson.Gson;

/**
 * Servlet implementation class CartsApi
 */
public class CartsApi extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartsApi() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
	    response.addHeader("Access-Control-Allow-Methods", "*");
	    response.addHeader("Access-Control-Allow-Headers", "http://localhost:3000/");
	    
	// TODO Auto-generated method stub
    CartDao dao=new CartDao();
    List<Cart> users=new ArrayList<>();
    users=dao.getCart();
	Gson gson = new Gson();
	String userJSON;
	try {
		userJSON = gson.toJson(users);
		PrintWriter pw=response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		pw.write(userJSON);
		pw.close();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.addHeader("Access-Control-Allow-Origin", "*");
	    response.addHeader("Access-Control-Allow-Methods", "*");
	    response.addHeader("Access-Control-Allow-Headers", "*");
	    
		CartDao cartDao =new CartDao();
		Cart prod = null;
		PrintWriter p =response.getWriter();
		
		int prod_id =Integer.parseInt(request.getParameter("prod_id"));
        int cart_id = Integer.parseInt(request.getParameter("cart_id"));
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        int user_id = Integer.parseInt(request.getParameter("user_id"));
        
        prod = new Cart(cart_id,quantity,prod_id, user_id );
		int upd = 404;

		int record = 0;		try {
			record = cartDao.cartitem(prod);
			if (record > 0) {
				// if record inserted 200
				upd = 200;
			} else {
				upd = 404;
			}
			String productJsonString = new Gson().toJson(upd);

			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			p.print(productJsonString);
			p.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	

}
