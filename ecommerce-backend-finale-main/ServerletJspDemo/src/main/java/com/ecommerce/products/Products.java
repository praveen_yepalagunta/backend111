package com.ecommerce.products;

public class Products {

    int id;
	
    
	String productName;
	double price;
	int qty;
	String prodDesc;
	int category_id;
	String image;
	
	
	public Products(int id, String productName, double price, int qty, String prodDesc, int category_id, String image) {
		super();
		this.id = id;
		this.productName = productName;
		this.price = price;
		this.qty = qty;
		this.prodDesc = prodDesc;
		this.category_id=category_id;
		this.image=image;
	}
	
	
	public int getCategory_id() {
		return category_id;
	}


	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

//
//	public Products(int id, String productName, double price, int qty, String prodDesc) {
//		super();
//		this.id = id;
//		this.productName = productName;
//		this.price = price;
//		this.qty = qty;
//		this.prodDesc = prodDesc;
////		this.image = image;
//	}
	public Products() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public String getProdDesc() {
		return prodDesc;
	}

	public void setProdDesc(String prodDesc) {
		this.prodDesc = prodDesc;
	}

	public double getStockValue() {
		return price * qty;
	}

	
	
	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	@Override
	public String toString() {
		return "Products [id=" + id + ", productName=" + productName + ", price=" + price + ", qty=" + qty
				+ ", prodDesc=" + prodDesc + ", category_id=" + category_id + ", image=" + image + "]";
	}

 
	 
}
