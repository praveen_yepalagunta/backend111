package com.ecommerce.CartDao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ecommerce.cart.Cart;
import com.ecommerce.products.Products;

public class CartDao {
	public int cartitem(Cart cart)throws Exception {
	int integer=0;
	
	    try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://db:3306/praveen","root","root");
			PreparedStatement ps = con.prepareStatement("insert into cart( quantity,prod_id, user_id) values(?,?,?)");
			System.out.println(cart);
			ps.setInt(1,cart.getUser_id());
			ps.setInt(2, cart.getQuantity());
			ps.setInt(3, cart.getProd_id());
			 
			integer=ps.executeUpdate();
			ps.close();
			return integer;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	
	return integer ;
}

	public List<Cart> getCart()
	{
		List<Cart> users=new ArrayList<>();
		Cart c = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/praveen", "root", "root");
			
			Statement stmt = con.createStatement();
			String s = ("SELECT * from cart");
			ResultSet rs = stmt.executeQuery(s);
			
			 while(rs.next()) {
				
				int order_id=rs.getInt("cart_id");
				int quantity=rs.getInt("quantity");
				int prod_id = rs.getInt("prod_id");
				int user_id = rs.getInt("user_id");
				c = new Cart(order_id,quantity,prod_id, user_id);
				users.add(c);
				
			}
		rs.close();
		stmt.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		
		return users;
		
		
		
	}
 
 

}
