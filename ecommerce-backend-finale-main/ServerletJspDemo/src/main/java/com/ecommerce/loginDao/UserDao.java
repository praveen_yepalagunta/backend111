package com.ecommerce.loginDao;


import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ecommerce.*;
import com.ecommerce.login.User;




public class UserDao {
	//UserDao userDaoObj = new UserDao();

	public User searchForUser(String username, String password) throws ClassNotFoundException {
		User user = null;
		Connection con = null;
		
		try 
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://db:3306/praveen", "root", "root");
				PreparedStatement ps = con.prepareStatement("select username,password from users where username=? and password=?");
				ps.setString(1, username);
				ps.setString(2, password);
				ResultSet rs = ps.executeQuery();

					if (rs.next()) 
					{
						user=new User(rs.getString("username"),rs.getString("password"));
						
						System.out.println("Valid user login, Login Successful!");
						//try 
//						{
//							URI uri = new URI("http://www.google.com");
//							Desktop.getDesktop().browse(uri);
//						}catch (URISyntaxException | IOException e) 
//						{e.printStackTrace();
//						}
					} 
					else if(rs.next()==false)
					{
						System.out.println("Invalid user login, kindly recheck your username or password. OR Sign-Up if you are a new user.");
						}
					
				rs.close();
				ps.close();
				return user;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return user;
	}
	
	
	public int registerUser(User user) throws ClassNotFoundException
	{
		
		Connection con = null;
		int record=0;
	
		try 
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://db:3306/praveen", "root", "root");
				PreparedStatement ps = con.prepareStatement("insert into users(user_id,username,password,email_id,address,user_role) values(?,?,?,?,?,?)");
				ps.setInt(1, user.getUser_id());
				ps.setString(2, user.getUsername());
				ps.setString(3, user.getPassword());
				ps.setString(4, user.getEmail_id());
			    ps.setString(5, user.getAddress());
			    ps.setString(6, user.getUser_role());
		 
				record=ps.executeUpdate();
				record=1;
						
				ps.close();
				return record;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return record;
	}
		
		
	}


