package com.ecommerce.web.dao;

import com.ecommerce.products.Products;
import com.ecommerce.web.*;
import java.io.*;
import com.ecommerce.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductsDao {

	public List<Products> getProducts(int cate) {
		List<Products> users = new ArrayList<>();
		Products p = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/praveen", "root", "root");
			PreparedStatement stmt = con.prepareStatement("SELECT * FROM products WHERE category_id=?");
			stmt.setInt(1, cate);
			ResultSet rs = stmt.executeQuery();
			System.out.println(rs.next());

			while (rs.next()) {
				int id = rs.getInt("prod_id");
				String name = rs.getString("prod_name");
				String desc = rs.getString("prod_desc");
				double price = rs.getDouble("prod_price");
				int Qty = rs.getInt("quantity");
				int Category_id = rs.getInt("category_id");
				String image = rs.getString("image");
				p = new Products(id, name, price, Qty, desc, Category_id, image);
				users.add(p);

			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

		return users;

	}

	public List<Products> getProducts1() {
		List<Products> users = new ArrayList<>();
		Products p = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/praveen", "root", "root");
			PreparedStatement stmt = con.prepareStatement("SELECT * FROM products");

			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("prod_id");
				String name = rs.getString("prod_name");
				String desc = rs.getString("prod_desc");
				double price = rs.getDouble("prod_price");
				int Qty = rs.getInt("quantity");
				int Category_id = rs.getInt("category_id");
				String image = rs.getString("image");
				p = new Products(id, name, price, Qty, desc, Category_id, image);
				users.add(p);

			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

		return users;

	}

	public int addInventory(Products prod) throws ClassNotFoundException {
		int record = 0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/praveen", "root", "root");
			System.out.println(prod);
			PreparedStatement ps = con.prepareStatement("insert into products values(?,?,?,?,?,?,?)");
			ps.setInt(1, prod.getId());
			ps.setString(2, prod.getProductName());
			ps.setString(3, prod.getProdDesc());
			ps.setDouble(4, prod.getPrice());
			ps.setInt(5, prod.getQty());
			ps.setInt(6, prod.getCategory_id());
			ps.setString(7, prod.getImage());
			record = ps.executeUpdate();
			ps.close();
			return record;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return record;
	}

	// public Products deleteProductUsingId(String prodId) throws
	// ClassNotFoundException {
	// Products prod=null;
	// try {
	// Class.forName("com.mysql.cj.jdbc.Driver");
	// Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/abdul",
	// "root", "root");
	//
	// PreparedStatement ps = con.prepareStatement("delete from products where
	// prod_id=?");
	// ps.setString(1, prodId);
	// int rs=ps.executeUpdate();
	// System.out.println("Row deleted sucessfully :"+rs);
	// ps.close();
	// } catch (SQLException e) {
	// e.printStackTrace();
	// }
	// return prod;
	// }
	// public int deleteProduct(int productId) throws ClassNotFoundException
	// {
	// int deleted=0;
	// try {
	// Class.forName("com.mysql.cj.jdbc.Driver");
	// Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/abdul",
	// "root", "root");
	// String sql = "delete from products where prod_Id = ?;";
	// PreparedStatement ps = con.prepareStatement(sql);
	// ps.setInt(1,productId);
	// ps.executeUpdate();
	// }catch(SQLException e)
	// {e.printStackTrace();
	// }
	// return deleted;
	// }
	public int deleteProduct(int prod_id) throws ClassNotFoundException {
		int delete = 0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/praveen", "root", "root");
			PreparedStatement ps = con.prepareStatement("delete from products where prod_id=?");
			ps.setInt(1, prod_id);

			delete = ps.executeUpdate();
			System.out.println(delete);
			ps.close();
			return delete;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return delete;
	}

	public int updateProductUsingId(int prodId, double price, String name, int qty, String prod_desc, String image) {
		int rs2 = 0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://db:3306/praveen", "root", "root");

			PreparedStatement ps2 = con.prepareStatement(
					"UPDATE Products SET prod_name=?,prod_desc=?, prod_price = ?,quantity=?, image=? WHERE prod_id=?");
			ps2.setString(1, name);
			ps2.setString(2, prod_desc);
			ps2.setDouble(3, price);
			ps2.setInt(4, qty);
			ps2.setInt(5, prodId);
			ps2.setString(6, image);
			rs2 = ps2.executeUpdate();
			System.out.println("The  product price has updated sucessfully" + rs2);
			ps2.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();

		}
		return rs2;
	}
}
